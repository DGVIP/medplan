import 'package:flutter/material.dart';
import 'const.dart';
import 'login.dart';

class MainChat extends StatelessWidget {
  static const strong1 = const Color(0xFF65D7DD);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chat',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: themeColor,
      ),
      home: Login(title: 'Chat'),
    );
  }
}

