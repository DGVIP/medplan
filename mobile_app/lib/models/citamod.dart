class Cita {
  final String doctor;
  final String time;
  final String text;
  final String date;

  Cita({this.doctor, this.time, this.date, this.text});
}

List<Cita> citas = [
  Cita(
    doctor: 'Dr. Diego Ramirez',
    time: '5:30 PM',
    date: '24/07/2020',
    text: 'Cita Oncología',
  ),
  Cita(
    doctor: 'Dr. Oscar Paredes',
    time: '4:15 PM',
    date: '06/07/2020',
    text: 'Cita Pediatría',
  ),
  Cita(
    doctor: 'Dra. Jazmín Cardenas',
    time: '9:30 AM',
    date: '05/06/2020',
    text: 'Cita Cardiología',
  ),
  Cita(
    doctor: 'Dr. Christian Velazco',
    time: '10:30 AM',
    date: '19/07/2020',
    text: 'Cita Urología',
  ),
  Cita(
    doctor: 'Dr. Manuel Navarrete',
    time: '11:30 AM',
    date: '03/07/2020',
    text: 'Cita Traumatología',
  ),
  Cita(
      doctor: 'Dra. Alison Vega',
      time: '3:30 PM',
      date: '16/07/2020',
      text: 'Cita Oftalmología'),
  Cita(
      doctor: 'Dr. Fernando Trillo',
      time: '11:30 AM',
      date: '11/07/2020',
      text: 'Cita Reumatología'),
  Cita(
      doctor: 'Dra. Karim Flores',
      time: '6:30 PM',
      date: '12/07/2020',
      text: 'Medicina General')
];
