import 'package:flutter/material.dart';
import 'const.dart';
import 'dart:async';
import 'dart:io';

import 'package:flutter_appavailability/flutter_appavailability.dart';

class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  List<Map<String, String>> _installedApps;
  static const bg = const Color(0xFFE9EBED);
  static const strong1 = const Color(0xFF65D7DD);
  static const strong2 = const Color(0xFF2997AE);
  static const emphasis = const Color(0xFF184A75);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bg,
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: strong1,
          title: Text(
            'MedPlan',
            style: TextStyle(
              color: emphasis,
              fontWeight: FontWeight.w700,
              fontSize: 25.0,
            ),
          ),
          centerTitle: true,
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.person),
              onPressed: () {
                Navigator.pushNamed(context, "/login");
              },
            ),
          ],
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          children: <Widget>[
            Container(
              height: 100,
              margin: EdgeInsets.symmetric(vertical: 8.0),
              child: _buildTile(
                Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Citas',
                                style:
                                    TextStyle(color: emphasis, fontSize: 30.0))
                          ],
                        ),
                        Material(
                            color: strong2,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center(
                                child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Icon(Icons.calendar_today,
                                  color: Colors.white, size: 30.0),
                            )))
                      ]),
                ),
                onTap: () {
                  Navigator.of(context).pushNamed("/citas");
                },
              ),
            ),
            Container(
              height: 100,
              margin: EdgeInsets.symmetric(vertical: 8.0),
              child: _buildTile(
                Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Chat',
                                style:
                                    TextStyle(color: emphasis, fontSize: 30.0))
                          ],
                        ),
                        Material(
                            color: strong2,
                            borderRadius: BorderRadius.circular(24.0),
                            child: Center(
                                child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Icon(Icons.chat,
                                  color: Colors.white, size: 30.0),
                            )))
                      ]),
                ),
                onTap: () {
                  Navigator.of(context).pushNamed("/chat");
                },
              ),
            ),
            Container(
                height: 100,
                margin: EdgeInsets.symmetric(vertical: 8.0),
                child: _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Ver mapa',
                                  style: TextStyle(
                                      color: emphasis, fontSize: 30.0))
                            ],
                          ),
                          Material(
                              color: strong2,
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center(
                                  child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.map,
                                    color: Colors.white, size: 30.0),
                              )))
                        ]),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed("/mapa");
                  },
                )),
            Container(
                height: 100,
                margin: EdgeInsets.symmetric(vertical: 8.0),
                child: _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Ver vídeo',
                                  style: TextStyle(
                                      color: emphasis, fontSize: 30.0))
                            ],
                          ),
                          Material(
                              color: strong2,
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center(
                                  child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.video_library,
                                    color: Colors.white, size: 30.0),
                              )))
                        ]),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed("/videoplayer");
                  },
                )),
            Container(
                height: 100,
                margin: EdgeInsets.symmetric(vertical: 8.0),
                child: _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Juego',
                                  style: TextStyle(
                                      color: emphasis, fontSize: 30.0))
                            ],
                          ),
                          Material(
                              color: strong2,
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center(
                                  child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.video_library,
                                    color: Colors.white, size: 30.0),
                              )))
                        ]),
                  ),
                  onTap: () {
                    if (Platform.isAndroid) {
                      AppAvailability.launchApp("org.godotengine.boxgame")
                          .then((_) {
                        print("App launched!");
                      }).catchError((err) {
                        Scaffold.of(context).showSnackBar(
                            SnackBar(content: Text("App not found!")));
                        print(err);
                      });
                    }
                  },
                )),
            Container(
                height: 100,
                margin: EdgeInsets.symmetric(vertical: 8.0),
                child: _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Salir',
                                  style: TextStyle(
                                      color: emphasis, fontSize: 30.0))
                            ],
                          ),
                          Material(
                              color: strong2,
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center(
                                  child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.exit_to_app,
                                    color: Colors.white, size: 30.0),
                              )))
                        ]),
                  ),
                  onTap: () {
                    openDialog();
                  },
                ))
          ],
        ));
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
      elevation: 14.0,
      borderRadius: BorderRadius.circular(12.0),
      shadowColor: Color(0x802196F3),
      child: InkWell(
        // Do onTap() if it isn't null, otherwise do print()
        onTap: onTap != null
            ? () => onTap()
            : () {
                print('Not set yet');
              },
        child: child,
      ),
    );
  }

  Future<Null> openDialog() async {
    switch (await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            contentPadding:
                EdgeInsets.only(left: 0.0, right: 0.0, top: 0.0, bottom: 0.0),
            children: <Widget>[
              Container(
                color: themeColor,
                margin: EdgeInsets.all(0.0),
                padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                height: 100.0,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.exit_to_app,
                        size: 30.0,
                        color: Colors.white,
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),
                    ),
                    Text(
                      'Salir de la aplicación',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '¿Seguro que desea salir?',
                      style: TextStyle(color: Colors.white70, fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 0);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.cancel,
                        color: primaryColor,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'Cancelar',
                      style: TextStyle(
                          color: primaryColor, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, 1);
                },
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Icon(
                        Icons.check_circle,
                        color: primaryColor,
                      ),
                      margin: EdgeInsets.only(right: 10.0),
                    ),
                    Text(
                      'Si',
                      style: TextStyle(
                          color: primaryColor, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
            ],
          );
        })) {
      case 0:
        break;
      case 1:
        exit(0);
        break;
    }
  }
}
