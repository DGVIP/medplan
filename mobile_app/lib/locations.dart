import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

Future<List<dynamic>> getHospitals(double latitude, double longitude) async {
  print(latitude.toString() + " " + longitude.toString());
  var hosp = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json' +
      '?location=$latitude,$longitude' +
      '&radius=1000&type=hospital' +
      '&key=AIzaSyCuTdy1DkA51a1P69raH_ovYl6Jg8oyTfM';
  final response = await http.get(hosp);
  var res = json.decode(response.body);
  if (response.statusCode == 200) {
    return res["results"];
  } else {
    throw HttpException(
        'Unexpected status code ${response.statusCode}:'
        ' ${response.reasonPhrase}',
        uri: Uri.parse(hosp));
  }
}
