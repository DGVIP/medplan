import 'dart:ui';

final themeColor = Color(0xFF65D7DD);
final primaryColor = Color(0xFF184A75);
final greyColor = Color(0xFF2997AE);
final greyColor2 = Color(0xffE8E8E8);
final text = Color(0xFFE9EBED);
