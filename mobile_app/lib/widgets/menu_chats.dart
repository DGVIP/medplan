import 'package:flutter/material.dart';

class MenuChats extends StatefulWidget {
  @override
  _MenuChatsState createState() => _MenuChatsState();
}

class _MenuChatsState extends State<MenuChats> {
  int indexselec = 0;
  final List<String> categorias = ['Mensajes', 'Contactos'];
  static const strong1 = const Color(0xFF65D7DD);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      color: strong1,
      child: Center(
        child: ListView.builder(
            physics: ClampingScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: categorias.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  setState(() {
                    indexselec = index;
                  });
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 40,
                    vertical: 15.0,
                  ),
                  child: Text(
                    categorias[index],
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color:
                          index == indexselec ? Colors.white : Colors.white60,
                      fontSize: 18.0,
                      letterSpacing: 3.0,
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
