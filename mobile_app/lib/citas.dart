import 'package:flutter/material.dart';
import 'package:mobile_app/widgets/citasformato.dart';

class Citas extends StatefulWidget {
  @override
  _CitasState createState() => _CitasState();
}

class _CitasState extends State<Citas> {
  static const bg = const Color(0xFFE9EBED);
  static const strong1 = const Color(0xFF65D7DD);
  static const emphasis = const Color(0xFF184A75);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bg,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: strong1,
        title: Text(
          'MedPlan',
          style: TextStyle(
            color: emphasis,
            fontWeight: FontWeight.w700,
            fontSize: 25.0,
          ),
        ),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                ),
              ),
              child: Column(
                children: <Widget>[
                  AssignedCitas(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
